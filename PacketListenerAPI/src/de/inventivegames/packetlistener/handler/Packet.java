/*
 * Copyright 2015 Marvin Sch�fer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.packetlistener.handler;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;

/**
 *
 * � Copyright 2015 inventivetalent
 *
 * @author inventivetalent
 *
 */
public abstract class Packet {

	private Player		player;
	private Object		packet;
	private Cancellable	cancel;

	public Packet(Object packet, Cancellable cancel, Player player) {
		this.player = player;
		this.packet = packet;
		this.cancel = cancel;
	}

	public void setPacketValue(String field, Object value) {
		try {
			Field f = packet.getClass().getDeclaredField(field);
			f.setAccessible(true);
			f.set(packet, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Object getPacketValue(String field) {
		Object value = null;
		try {
			Field f = packet.getClass().getDeclaredField(field);
			f.setAccessible(true);
			value = f.get(packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public void setCancelled(boolean b) {
		cancel.setCancelled(b);
	}

	public boolean isCancelled() {
		return cancel.isCancelled();
	}

	public Player getPlayer() {
		return player;
	}

	public boolean hasPlayer() {
		return player != null;
	}

	public String getPlayername() {
		if (!hasPlayer()) return null;
		return player.getName();
	}

	public void setPacket(Object packet) {
		this.packet = packet;
	}

	public Object getPacket() {
		return packet;
	}

	public String getPacketName() {
		return packet.getClass().getSimpleName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (cancel == null ? 0 : cancel.hashCode());
		result = prime * result + (packet == null ? 0 : packet.hashCode());
		result = prime * result + (player == null ? 0 : player.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Packet other = (Packet) obj;
		if (cancel == null) {
			if (other.cancel != null) return false;
		} else if (!cancel.equals(other.cancel)) return false;
		if (packet == null) {
			if (other.packet != null) return false;
		} else if (!packet.equals(other.packet)) return false;
		if (player == null) {
			if (other.player != null) return false;
		} else if (!player.equals(other.player)) return false;
		return true;
	}

}
